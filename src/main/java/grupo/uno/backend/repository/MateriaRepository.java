package grupo.uno.backend.repository;

import grupo.uno.backend.model.Materia;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MateriaRepository extends JpaRepository <Materia, Long> {
    public Materia getMateriaByNombreMateriaIsLike(String nombreMateria);
}
