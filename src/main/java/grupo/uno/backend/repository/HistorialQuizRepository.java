package grupo.uno.backend.repository;

import grupo.uno.backend.model.HistorialQuiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistorialQuizRepository extends JpaRepository<HistorialQuiz, Long> {
}
