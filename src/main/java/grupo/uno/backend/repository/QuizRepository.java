package grupo.uno.backend.repository;

import grupo.uno.backend.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizRepository extends JpaRepository <Quiz, Long> {
}
