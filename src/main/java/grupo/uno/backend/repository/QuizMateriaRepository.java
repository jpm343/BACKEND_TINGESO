package grupo.uno.backend.repository;

import grupo.uno.backend.model.QuizMateria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizMateriaRepository extends JpaRepository <QuizMateria, Long> {
}
