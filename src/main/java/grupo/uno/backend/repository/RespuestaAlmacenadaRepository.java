package grupo.uno.backend.repository;

import grupo.uno.backend.model.RespuestaAlmacenada;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaAlmacenadaRepository extends JpaRepository<RespuestaAlmacenada, Long> {
}
