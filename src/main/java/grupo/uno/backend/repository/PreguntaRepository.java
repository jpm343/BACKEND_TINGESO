package grupo.uno.backend.repository;

import grupo.uno.backend.model.Pregunta;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PreguntaRepository extends JpaRepository <Pregunta, Long> {
}
