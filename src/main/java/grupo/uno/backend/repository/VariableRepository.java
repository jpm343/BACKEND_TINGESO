package grupo.uno.backend.repository;

import grupo.uno.backend.model.Variable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableRepository extends JpaRepository<Variable, Long> {
}
