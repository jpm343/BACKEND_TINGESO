package grupo.uno.backend.controller;

import grupo.uno.backend.model.Materia;
import grupo.uno.backend.model.Pregunta;
import grupo.uno.backend.repository.MateriaRepository;
import grupo.uno.backend.repository.PreguntaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Validated
@CrossOrigin(origins = "*")
public class PreguntaController {
    @Autowired
    PreguntaRepository preguntaRepository;

    @Autowired
    MateriaRepository materiaRepository;

    //obtener todas las preguntas
    @RequestMapping(value = "/preguntas", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll() {
        return new ResponseEntity( preguntaRepository.findAll(), HttpStatus.OK);
    }

    //crear una pregunta
    @RequestMapping(value = "/crear_pregunta", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity create(@RequestBody Pregunta resource) {
        Materia valorMateria;
        Optional<Materia> valor = materiaRepository.findById(resource.getMateriaId());
        if(valor.isPresent())
            valorMateria = valor.get();
        else
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        resource.setMateria(valorMateria);//tricky
        return new ResponseEntity( preguntaRepository.save(resource), HttpStatus.CREATED);
    }

    //borrar una pregunta
    @RequestMapping(value = "/borrar_pregunta_{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(@PathVariable("id") Long id) {
        Pregunta valorPregunta;
        Optional<Pregunta> valor = preguntaRepository.findById(id);
        if(valor.isPresent()) {
            valorPregunta = valor.get();
            preguntaRepository.delete(valorPregunta);
            return new ResponseEntity( preguntaRepository.findAll(), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }
}
