package grupo.uno.backend.controller;

import grupo.uno.backend.model.HistorialQuiz;
import grupo.uno.backend.model.RespuestaAlmacenada;
import grupo.uno.backend.repository.HistorialQuizRepository;
import grupo.uno.backend.repository.RespuestaAlmacenadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Validated
@CrossOrigin (origins = "*")
@RequestMapping (value = "/respuestas")
public class RespuestaAlmacenadaController {
    @Autowired
    private RespuestaAlmacenadaRepository respuestaAlmacenadaRepository;
    @Autowired
    private HistorialQuizRepository historialQuizRepository;

    //obtener todas las respuestas
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll() {
        return new ResponseEntity(respuestaAlmacenadaRepository.findAll(), HttpStatus.OK);
    }

    //crear una respuesta y asociarla a un historial
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity create(@RequestBody RespuestaAlmacenada resource) {
        HistorialQuiz valorHistorialQuiz;
        Optional<HistorialQuiz> valor = historialQuizRepository.findById(resource.getHistorialQuizId());
        if(valor.isPresent())
            valorHistorialQuiz = valor.get();
        else
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        resource.setHistorialQuiz(valorHistorialQuiz);
        return new ResponseEntity(respuestaAlmacenadaRepository.save(resource), HttpStatus.CREATED);

    }

    //actualizar una respuesta (anadir respuesta de usuario)
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody RespuestaAlmacenada resource) {
        RespuestaAlmacenada valorRespuestaAlmacenada;
        Optional<RespuestaAlmacenada> valor = respuestaAlmacenadaRepository.findById(id);
        if(valor.isPresent()) {
            valorRespuestaAlmacenada = valor.get();
            valorRespuestaAlmacenada.setRespuestaAlumno(resource.getRespuestaAlumno());
            valorRespuestaAlmacenada.setNumeroRespuesta(resource.getNumeroRespuesta());
            valorRespuestaAlmacenada.setPutajeRespuesta(resource.getPutajeRespuesta());
            String respuestaRealStrip = valorRespuestaAlmacenada.getRespuestaReal().trim();
            if(respuestaRealStrip.equals(valorRespuestaAlmacenada.getRespuestaAlumno())) {
                valorRespuestaAlmacenada.setPutajeRespuesta(2F);
            }
            else
                valorRespuestaAlmacenada.setPutajeRespuesta(0F);
            Float puntaje = resource.getPutajeRespuesta();
            if(puntaje != null)
                valorRespuestaAlmacenada.setPutajeRespuesta(puntaje);

            return new ResponseEntity(respuestaAlmacenadaRepository.save(valorRespuestaAlmacenada), HttpStatus.CREATED);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
