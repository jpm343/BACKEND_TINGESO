package grupo.uno.backend.controller;

import grupo.uno.backend.model.HistorialQuiz;
import grupo.uno.backend.model.Quiz;
import grupo.uno.backend.repository.HistorialQuizRepository;
import grupo.uno.backend.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Validated
@CrossOrigin(origins = "*")
@RequestMapping(value = "/historial")
public class HistorialQuizController {
    @Autowired
    private HistorialQuizRepository historialQuizRepository;
    @Autowired
    private QuizRepository quizRepository;

    //la idea es que se cree un historial al momento de iniciar el quiz.
    //el historial quiz (o al menos la id) debe quedar almacenado en algun estado
    //a medida que se responden las preguntas, se van asociando al historial

    //obtener todos los historiales
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll() {
        return new ResponseEntity(historialQuizRepository.findAll(), HttpStatus.OK);
    }

    //crear un historial y asociarlo a un quiz
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity create(@RequestBody HistorialQuiz resource) {
        Quiz valorQuiz;
        Optional<Quiz> valor = quizRepository.findById(resource.getQuizId());
        if(valor.isPresent())
            valorQuiz = valor.get();
        else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        resource.setQuiz(valorQuiz);
        return new ResponseEntity(historialQuizRepository.save(resource), HttpStatus.CREATED);
    }

    //obtener por id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getById(@PathVariable("id") Long id) {
        HistorialQuiz valorHistorialQuiz;
        Optional<HistorialQuiz> valor = historialQuizRepository.findById(id);
        if(valor.isPresent()) {
            valorHistorialQuiz = valor.get();
            return new ResponseEntity(valorHistorialQuiz, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
