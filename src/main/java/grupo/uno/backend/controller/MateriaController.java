package grupo.uno.backend.controller;

import grupo.uno.backend.Respuesta;
import grupo.uno.backend.model.*;
import grupo.uno.backend.repository.HistorialQuizRepository;
import grupo.uno.backend.repository.MateriaRepository;
import grupo.uno.backend.repository.QuizMateriaRepository;
import grupo.uno.backend.repository.RespuestaAlmacenadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@Validated
@CrossOrigin(origins = "*")
public class MateriaController {
    @Autowired
    private MateriaRepository materiaRepository;
    @Autowired
    private HistorialQuizRepository historialQuizRepository; //aqui se guardan las respuestas
    @Autowired
    private RespuestaAlmacenadaRepository respuestaAlmacenadaRepository;
    @Autowired
    private QuizMateriaRepository quizMateriaRepository;


    //obtener todas las materias
    @RequestMapping(value = "/materias", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll() {
        return new ResponseEntity(materiaRepository.findAll(), HttpStatus.OK);
    }

    //obtener materia por nombre (o parecido al nombre)
    @RequestMapping(value = "/materia_{nombre}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getByName(@PathVariable("nombre") String nombre) {
        return new ResponseEntity(materiaRepository.getMateriaByNombreMateriaIsLike(nombre), HttpStatus.OK);
    }

    //obtener materia por id
    @RequestMapping(value = "/materia/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getById(@PathVariable("id") Long id) {
        return new ResponseEntity(materiaRepository.getOne(id), HttpStatus.OK);
    }

    //obtener materia por id de quiz_materia
    @RequestMapping(value = "materia/quiz/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getByQuizMateriaId(@PathVariable("id") Long id) {
        QuizMateria valorQuizMateria;
        Optional<QuizMateria> valor = quizMateriaRepository.findById(id);
        if(valor.isPresent())
            valorQuizMateria = valor.get();
        else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        Materia materia;
        materia = valorQuizMateria.getMateria();
        return new ResponseEntity(materia, HttpStatus.OK);
    }

    //evaluacion 4 diciembre
    //crear una materia
    @RequestMapping(value = "/crearMateria", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity create(@RequestBody Materia resource) {
        return new ResponseEntity(materiaRepository.save(resource), HttpStatus.CREATED);
    }

    //obtener una pregunta aleatoria por materia
    //probablemente aqui es donde se le asigna la respuesta a la pregunta
    //cada vez que la pregunta es seleccionada, su respuesta es actualizada
    @RequestMapping(value = "/obtener_pregunta_{materia_id}/{historial_id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getPreguntaRandom(@PathVariable("materia_id") Long materiaId, @PathVariable("historial_id") Long historialId) {
        Materia valorMateria;
        HistorialQuiz valorHistorialQuiz;

        Optional<Materia> valorM = materiaRepository.findById(materiaId);
        Optional<HistorialQuiz> valorQ = historialQuizRepository.findById(historialId);

        if(valorM.isPresent() && valorQ.isPresent()) {
            valorMateria = valorM.get();
            valorHistorialQuiz = valorQ.get();
        }
        else
            return null;

        List<Pregunta> seleccionadas = valorMateria.getPreguntas();
        Random rand = new Random();
        if (!seleccionadas.isEmpty()) { //se verifica que hayan preguntas en el pozo
            int indiceRandom = rand.nextInt(seleccionadas.size());
            Pregunta seleccionada = seleccionadas.get(indiceRandom);

            //aca se crea una nueva respuesta almacenada
            RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
            String codigoUnico = seleccionada.getPregunta();
            codigoUnico = shuffleParameters(seleccionada, codigoUnico);
            respuestaAlmacenada.setRespuestaReal( getRespuesta(codigoUnico) );
            //shuffleParameters(seleccionada);
            respuestaAlmacenada.setCodigo(codigoUnico);
            respuestaAlmacenada.setHistorialQuiz(valorHistorialQuiz);
            respuestaAlmacenadaRepository.save(respuestaAlmacenada);

            return new ResponseEntity(codigoUnico, HttpStatus.OK);
        } else { // caso en el que no hay preguntas en el pozo
            return null;
        }
    }

    //envio de codigo a la API
    private static String getRespuesta(String codigo) {
        final String uri = "https://run.glot.io/languages/python/latest";
        String json = "{\"files\": [{\"name\": \"main.py\", \"content\": \"" + codigo + "\"}]}";
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", Materia.API_TOKEN);
        headers.add("Content-Type", "application/json");

        HttpEntity<String> entity =  new HttpEntity<>(json, headers);

        Respuesta result = restTemplate.postForObject(uri, entity, Respuesta.class);

        //asumiendo que cada pregunta tiene respuesta valida
        return result.getStdout();
    }

    //variacion de paramteros deberia ser aqui
    private static String shuffleParameters(Pregunta pregunta, String codigo) {
        List<Variable> variables = pregunta.getVariables();
        if(variables.isEmpty())
            return codigo;

        String porcionCodigo = "";
        Random rand = new Random();
        for(Variable variable: variables) {
            int valorVariable = rand.nextInt(100);
            porcionCodigo = porcionCodigo.concat(variable.getNombreVariable()+"="+valorVariable+"\n");
        }
        String codigoFinal = porcionCodigo + "\n" + codigo;
        return codigoFinal;
    }
}