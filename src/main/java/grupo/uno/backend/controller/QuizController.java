package grupo.uno.backend.controller;

import grupo.uno.backend.Puntaje;
import grupo.uno.backend.model.Materia;
import grupo.uno.backend.model.Quiz;
import grupo.uno.backend.model.QuizMateria;
import grupo.uno.backend.repository.MateriaRepository;
import grupo.uno.backend.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Validated
@CrossOrigin(origins = "*")
public class QuizController {
    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private MateriaRepository materiaRepository;

    //obtener todos los quizzes
    @RequestMapping(value = "/quizzes", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getAll() {
        return new ResponseEntity(quizRepository.findAll(), HttpStatus.OK);
    }

    //crear un quizz (recibe unidades) ojo con este
    @RequestMapping(value = "/crear_quizz", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity create(@RequestBody Quiz resource) {
        return new ResponseEntity(quizRepository.save(resource), HttpStatus.CREATED);
    }

    //habilitar o deshabilitar un quizz
    @RequestMapping(value = "/habilitar_deshabilitar_quizz/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity habilitar(@PathVariable("id") long id) {
        Quiz valorQuiz;
        Optional<Quiz> valor = quizRepository.findById(id);
        if(valor.isPresent()) {
            valorQuiz = valor.get();

            if(valorQuiz.isHabilitado())
                valorQuiz.setHabilitado(false);
            else
                valorQuiz.setHabilitado(true);

            quizRepository.save(valorQuiz);
        }
        //hola
        return new ResponseEntity(quizRepository.findById(id), HttpStatus.OK);
    }

    //agregar materias a un quiz
    @RequestMapping(value = "/quizz/{quizz_id}/materia/{materia_id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity anadir(@PathVariable("quizz_id") Long quizId, @PathVariable("materia_id") Long materiaId, @RequestBody Puntaje resource) {
        Materia valorMateria;
        Quiz valorQuiz;

        Optional<Materia> valorM = materiaRepository.findById(materiaId);
        Optional<Quiz> valorQ = quizRepository.findById(quizId);

        if(valorM.isPresent() && valorQ.isPresent()) {
            valorMateria = valorM.get();
            valorQuiz = valorQ.get();

            if (valorQuiz.cantidadMaterias() < Quiz.MAX_PREGUNTAS) {
                QuizMateria quizMateria= new QuizMateria();
                quizMateria.setMateria(valorMateria);
                quizMateria.setQuiz(valorQuiz);
                quizMateria.setPuntajePregunta(resource.getPuntaje());
                valorQuiz.addMateria(quizMateria);
                //valorQuiz.addMateria(valorMateria);
                quizRepository.save(valorQuiz);
                return new ResponseEntity(quizRepository.findAll(), HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
