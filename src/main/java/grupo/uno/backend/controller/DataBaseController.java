package grupo.uno.backend.controller;

import grupo.uno.backend.model.Materia;
import grupo.uno.backend.model.Pregunta;
import grupo.uno.backend.model.Quiz;
import grupo.uno.backend.model.QuizMateria;
import grupo.uno.backend.repository.MateriaRepository;
import grupo.uno.backend.repository.PreguntaRepository;
import grupo.uno.backend.repository.QuizMateriaRepository;
import grupo.uno.backend.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

@RestController
@RequestMapping(value="/database")
@CrossOrigin(origins = "*")
public class DataBaseController {
    @Autowired
    MateriaRepository materiaRepository;
    @Autowired
    QuizRepository quizRepository;
    @Autowired
    PreguntaRepository preguntaRepository;
    @Autowired
    QuizMateriaRepository quizMateriaRepository;

    @RequestMapping(value="/seed", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity seed() {
        //seed de pozos y sus preguntas (3 por pozo)
        Materia prints1 = new Materia();
        prints1.setNombreMateria("prints_pozo1");
        materiaRepository.save(prints1);

        Pregunta pregunta1 = new Pregunta();
        pregunta1.setEstado(true);
        pregunta1.setMateria(prints1);
        pregunta1.setPregunta("print(1)");
        preguntaRepository.save(pregunta1);

        Pregunta pregunta2 = new Pregunta();
        pregunta2.setEstado(true);
        pregunta2.setMateria(prints1);
        pregunta2.setPregunta("print(2)");
        preguntaRepository.save(pregunta2);

        Pregunta pregunta3 = new Pregunta();
        pregunta3.setEstado(true);
        pregunta3.setMateria(prints1);
        pregunta3.setPregunta("print(3)");
        preguntaRepository.save(pregunta3);
        materiaRepository.save(prints1);


        Materia prints2 = new Materia();
        prints2.setNombreMateria("prints_pozo2");
        materiaRepository.save(prints2);

        Pregunta pregunta4 = new Pregunta();
        pregunta4.setEstado(true);
        pregunta4.setMateria(prints2);
        pregunta4.setPregunta("print(4)");
        preguntaRepository.save(pregunta4);

        Pregunta pregunta5 = new Pregunta();
        pregunta5.setEstado(true);
        pregunta5.setMateria(prints2);
        pregunta5.setPregunta("print(5)");
        preguntaRepository.save(pregunta5);

        Pregunta pregunta6 = new Pregunta();
        pregunta6.setEstado(true);
        pregunta6.setMateria(prints2);
        pregunta6.setPregunta("print(6)");
        preguntaRepository.save(pregunta6);
        materiaRepository.save(prints2);


        Materia prints3 = new Materia();
        prints3.setNombreMateria("prints_pozo3");
        materiaRepository.save(prints3);

        Pregunta pregunta7 = new Pregunta();
        pregunta7.setEstado(true);
        pregunta7.setMateria(prints3);
        pregunta7.setPregunta("print(7)");
        preguntaRepository.save(pregunta7);

        Pregunta pregunta8 = new Pregunta();
        pregunta8.setEstado(true);
        pregunta8.setMateria(prints3);
        pregunta8.setPregunta("print(8)");
        preguntaRepository.save(pregunta8);

        Pregunta pregunta9 = new Pregunta();
        pregunta9.setEstado(true);
        pregunta9.setMateria(prints3);
        pregunta9.setPregunta("print(9)");
        preguntaRepository.save(pregunta9);
        materiaRepository.save(prints3);

        //seed de quizzes y sus pozos asociados(solo 2)
        ArrayList<Materia> pozos1 = new ArrayList<>();
        pozos1.add(prints1);
        pozos1.add(prints2);
        pozos1.add(prints3);

        Quiz quiz1 = new Quiz();
        String dateString = "2019-12-12";
        java.sql.Date sqlStartDate;
        try{
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = sdf1.parse(dateString);
            sqlStartDate = new java.sql.Date(date.getTime());
            quiz1.setFechaApertura(sqlStartDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        quiz1.setHabilitado(true);
        quiz1.setNombreQuiz("quiz 1");
        quiz1.setNumeroQuiz(1);
        quiz1.setMinutosDuracion(15);
        quizRepository.save(quiz1);

        for(Materia pozo: pozos1){
            QuizMateria quizMateria= new QuizMateria();
            quizMateria.setPuntajePregunta(2.5F);
            //System.out.println(quizMateria);
            quizMateria.setMateria(pozo);
            quizMateria.setQuiz(quiz1);
            quiz1.addMateria(quizMateria);
            quizMateriaRepository.save(quizMateria);

            //System.out.println(quizMateria);
            quizRepository.save(quiz1);
        }


        //quiz1.setMaterias(pozos1);


        return new ResponseEntity(HttpStatus.OK);
    }
}
