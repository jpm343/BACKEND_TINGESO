package grupo.uno.backend.controller;

import grupo.uno.backend.model.Pregunta;
import grupo.uno.backend.model.Variable;
import grupo.uno.backend.repository.PreguntaRepository;
import grupo.uno.backend.repository.VariableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Validated
@CrossOrigin(origins = "*")
//para crear una variable y asociarla de inmediato a una pregunta
public class VariableController {
    @Autowired
    private VariableRepository variableRepository;
    @Autowired
    private PreguntaRepository preguntaRepository;

    //crear una variable y asociarla a una pregunta
    @RequestMapping(value = "/variable/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity create(@RequestBody Variable resource) {
        if(resource.getNombreVariable().isEmpty()) {
            return new ResponseEntity(HttpStatus.OK);
        }
        if(isReservedWord(resource.getNombreVariable())) {
            return new ResponseEntity(HttpStatus.OK);
        }

        Pregunta valorPregunta;
        Optional<Pregunta> valor = preguntaRepository.findById(resource.getPreguntaId());
        if(valor.isPresent())
            valorPregunta = valor.get();
        else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        resource.setPregunta(valorPregunta);
        return new ResponseEntity(variableRepository.save(resource), HttpStatus.CREATED);
    }

    private Boolean isReservedWord(String variable) {
        int i;
        for(i=0; i<Variable.reservadas.length; i++){
            if(variable.equals(Variable.reservadas[i]))
                return true;
        }
        return false;
    }
}
