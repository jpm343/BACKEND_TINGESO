package grupo.uno.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "materias")
public class Materia {
    public static final String API_TOKEN = "Token a733053a-3e0a-4d6e-a6a9-392a5667e26a";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "materia_id")
    private long materiaId;

    @Column(name = "nombreMateria")
    private String nombreMateria;

    //una materia tiene muchas preguntas
    //Viernes 28 de Diciembre
    @OneToMany(targetEntity = Pregunta.class, mappedBy = "materia",cascade = CascadeType.ALL)
    @JsonBackReference("materia-preguntas")
    private List<Pregunta> preguntas;

    //muhas materias pertenecen a muchos controles
    @OneToMany(mappedBy = "materia")
    @JsonBackReference("quiz-materia")
    private List<QuizMateria> QuizMateriaList;

    /*public void addQuiz(Quiz quiz) {
        this.quizzes.add(quiz);
    }*/
}
