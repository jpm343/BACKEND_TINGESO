package grupo.uno.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Getter
@Setter
@Table(name = "quiz_materia")
public class QuizMateria {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    @JsonBackReference("quiz")
    private Quiz quiz;

    @ManyToOne
    @JoinColumn(name = "materia_id")
    @JsonBackReference("quiz-materia")
    private Materia materia;

    //extra column
    @Column(name = "puntaje_pregunta")
    private Float puntajePregunta;
}
