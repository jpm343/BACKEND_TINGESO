package grupo.uno.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name =  "quizzes")
public class Quiz {
    public static final int MAX_PREGUNTAS = 3; //variable global

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "quiz_id")
    private long quizId;

    @Column(name = "nombre_quiz")
    private String nombreQuiz;

    @Column(name = "numero_quiz")//priemero, segundo, tercero, etc
    private int numeroQuiz;

    @Column(name = "fecha_apertura", nullable = false)
    private Date fechaApertura;

    @Column(name = "habilitado")//2 estados
    private boolean habilitado;

    @Column(name = "minutos_duracion")
    private int minutosDuracion;

    //por ahora solo se relaciona con materia
    //muchos quizzes tienen muchas materias
    @OneToMany(targetEntity = QuizMateria.class, mappedBy = "quiz", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<QuizMateria> quizMateriaList;

    /*@ManyToMany
    @JoinTable(name="quiz_materia", joinColumns = @JoinColumn(name="quiz_id"), inverseJoinColumns = @JoinColumn(name="materia_id"))
    private List<Materia> materias;*/

    //un quiz esta asociado a muchos historiales
    @OneToMany(targetEntity = HistorialQuiz.class, mappedBy = "quiz", cascade = CascadeType.ALL)
    @JsonBackReference("historial-quiz")
    private List<HistorialQuiz> historialQuizList;

    //obtener cantidad de materias de un quiz
    public int cantidadMaterias() {
        return this.quizMateriaList.size();
    }

    //anadir una materia a un quiz

    public void addMateria(QuizMateria quizMateria){
        if(this.quizMateriaList == null)
            this.setQuizMateriaList(new ArrayList<>());
        this.quizMateriaList.add(quizMateria);
    }
}
