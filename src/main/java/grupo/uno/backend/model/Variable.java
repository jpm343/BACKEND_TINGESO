package grupo.uno.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Getter
@Setter
@Table(name = "variables")
public class Variable {
    public static final int VALOR_MAX_VARIABLE=100;
    public static final String[] reservadas = {"False","class","finally","is","return","None","continue","for","lambda",
                                                "try","True","def","from","nonlocal","while","and","del","global","not",
                                                "with","as","elif","if","or","yield","assert","else","import","pass",
                                                "break","except","in","raise"};

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "variable_id")
    private long id;

    @Column(name = "nombre_variable")
    private String nombreVariable;

    //relaciones
    //muchas variables pertenecen a una pregunta
    @ManyToOne
    @JoinColumn(name = "pregunta_id")
    @JsonBackReference("pregunta-variable")
    private Pregunta pregunta;

    @Transient
    private Long preguntaId;
}
