package grupo.uno.backend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "preguntas")
public class Pregunta {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "pregunta_id")
    private long preguntaId;

    @Column(name = "pregunta") //se almacena el codigo python
    private String pregunta;

    @Column(name = "estado")
    private boolean estado; //publicada o no publicada

    @Column(name = "cantidad_variables")
    private int cantidadVariables;

    //llave foranea es transient
    @Transient
    private Long materiaId;

    //relaciones
    //muchas preguntas corresponden a una materia
    @ManyToOne
    @JoinColumn(name = "materia_id")
    private Materia materia;

    //una pregunta tiene muchas variables
    @OneToMany(targetEntity = Variable.class, mappedBy = "pregunta", cascade = CascadeType.ALL)
    private List<Variable> variables;

}
