package grupo.uno.backend.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="historial_quizzes")
public class HistorialQuiz {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="historial_id")
    private Long historialId;

    @Column(name = "fecha_almacenamiento", nullable = false, updatable = false)
    @CreationTimestamp
    private Date fechaAlmacenamiento;

    //relaciones (esta entidad manda todas las relaciones)
    //un historial tiene muchas respuestas almacenadas
    @OneToMany(targetEntity = RespuestaAlmacenada.class, mappedBy = "historialQuiz", cascade = CascadeType.ALL)
    private List<RespuestaAlmacenada> respuestaAlmacenadaList;

    //muchos historiales pertenecen a un quiz
    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    //foreign
    @Transient
    private long quizId;
}
