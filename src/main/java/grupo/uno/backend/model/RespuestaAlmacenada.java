package grupo.uno.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="respuestas_almacenadas")
public class RespuestaAlmacenada {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long respuestaAlmacenadaId;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "respuesta_real")
    private String respuestaReal;

    @Column(name = "respuesta_alumno")
    private String respuestaAlumno;

    @Column(name = "numero_respuesta")
    private int numeroRespuesta;

    @Column(name = "puntaje_respuesta")
    private Float putajeRespuesta;

    //relaciones
    //muchas respuestas almacenadas corresponden a un historial
    @ManyToOne
    @JoinColumn(name = "historial_id")
    @JsonBackReference("historial-respuestaAlmacenada")
    private HistorialQuiz historialQuiz;

    //para asociar directamente una respuesta a un historial
    @Transient
    private Long historialQuizId;
}
