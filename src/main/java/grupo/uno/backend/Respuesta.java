package grupo.uno.backend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Respuesta {
    private String stdout;
    private String stderr;
    private String error;
}
