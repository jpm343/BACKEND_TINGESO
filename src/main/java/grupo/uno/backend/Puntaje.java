package grupo.uno.backend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Puntaje {
    private Float puntaje;
}
