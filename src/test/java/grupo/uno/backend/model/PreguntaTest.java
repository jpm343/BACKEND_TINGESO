package grupo.uno.backend.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PreguntaTest {
    private Pregunta pregunta;

    @Before
    public void setUp() throws Exception {
        pregunta = new Pregunta();
        pregunta.setPreguntaId(1);
        pregunta.setPregunta("print(1)");
        pregunta.setMateria(new Materia());
        pregunta.setEstado(true);
    }

    @Test
    public void getPregunta() {
        assertEquals("print(1)", pregunta.getPregunta());
    }

    @Test
    public void getMateria() {
        assertEquals(pregunta.getMateria().getClass(), Materia.class);
    }

    @Test
    public void getPregunta_id() {
        assertEquals(1, pregunta.getPreguntaId());
    }

    @Test
    public void isEstado() {
        assertTrue(pregunta.isEstado());
    }

    @Test
    public void setPregunta_id() {
        pregunta.setPreguntaId(2);
        assertEquals(2, pregunta.getPreguntaId());
    }

    @Test
    public void setPregunta() {
        pregunta.setPregunta("print(2)");
        assertEquals("print(2)", pregunta.getPregunta());
    }

    @Test
    public void setEstado() {
        pregunta.setEstado(false);
        assertTrue(!pregunta.isEstado());
    }
}