package grupo.uno.backend.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class QuizTest {
    private Quiz quiz;

    @Before
    public void setUp() throws Exception {
        quiz = new Quiz();
        quiz.setHabilitado(true);
        //quiz.setMaterias(new ArrayList<Materia>());
        quiz.setNumeroQuiz(1);
        quiz.setQuizId(1);
    }

    /*
    @Test
    public void getMaterias() {
        assertTrue(quiz.getMaterias().isEmpty());
    }

    @Test
    public void setMaterias() {
        List<Materia> materias = quiz.getMaterias();
        materias.add(new Materia());
        quiz.setMaterias(materias);
        assertTrue(!quiz.getMaterias().isEmpty());
    }
    */

    @Test
    public void isHabilitado() {
        assertTrue(quiz.isHabilitado());
    }

    @Test
    public void setHabilitado() {
        quiz.setHabilitado(false);
        assertTrue(!quiz.isHabilitado());
    }

    @Test
    public void getQuiz_id() {
        assertEquals(1, quiz.getQuizId());
    }

    @Test
    public void getNumero_quiz() {
        assertEquals(1, quiz.getNumeroQuiz());
    }

    @Test
    public void setQuiz_id() {
        quiz.setQuizId(2);
        assertEquals(2, quiz.getQuizId());
    }

    @Test
    public void setNumero_quiz() {
        quiz.setNumeroQuiz(2);
        assertEquals(2, quiz.getNumeroQuiz());
    }
}