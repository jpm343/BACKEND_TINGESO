package grupo.uno.backend.model;

import grupo.uno.backend.Respuesta;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class HistorialQuizTest {
    @Test
    public void setHistorialId() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        historialQuiz.setHistorialId(1L);
        assertEquals((Long)1L, historialQuiz.getHistorialId());
    }

    @Test
    public void setRespuestaAlmacenadaList() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        historialQuiz.setRespuestaAlmacenadaList(new ArrayList<RespuestaAlmacenada>());
        assertTrue(historialQuiz.getRespuestaAlmacenadaList().isEmpty());
    }

    @Test
    public void setQuiz() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        Quiz quiz = new Quiz();
        historialQuiz.setQuiz(quiz);
        assertNotNull(historialQuiz.getQuiz());
    }

    @Test
    public void getHistorialId() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        historialQuiz.setHistorialId(1L);
        assertEquals((Long)1L, historialQuiz.getHistorialId());
    }

    @Test
    public void getRespuestaAlmacenadaList() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        historialQuiz.setRespuestaAlmacenadaList(new ArrayList<RespuestaAlmacenada>());
        assertTrue(historialQuiz.getRespuestaAlmacenadaList().isEmpty());
    }

    @Test
    public void getQuiz() {
        HistorialQuiz historialQuiz = new HistorialQuiz();
        Quiz quiz = new Quiz();
        historialQuiz.setQuiz(quiz);
        assertNotNull(historialQuiz.getQuiz());
    }
}