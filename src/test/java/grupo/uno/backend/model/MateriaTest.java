package grupo.uno.backend.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class MateriaTest {
    private Materia materia;

    @Before
    public void setUp() throws Exception {
        materia = new Materia();
        materia.setMateriaId(1);
        materia.setNombreMateria("prueba");
        materia.setPreguntas(new ArrayList<Pregunta>());
    }

    @Test
    public void getPreguntas() {
        List<Pregunta> preguntas = materia.getPreguntas();
        assertTrue(preguntas.isEmpty());
    }

    @Test
    public void setPreguntas() {
        List<Pregunta> preguntas = materia.getPreguntas();
        preguntas.add(new Pregunta());
        materia.setPreguntas(preguntas);
        assertTrue(!materia.getPreguntas().isEmpty());
    }

    @Test
    public void getMateria_id() {
        assertTrue(materia.getMateriaId() > 0);
    }

    @Test
    public void getNombreMateria() {
        assertEquals("prueba", materia.getNombreMateria());
    }

    @Test
    public void setNombreMateria() {
        materia.setNombreMateria("prueba2");
        assertEquals("prueba2", materia.getNombreMateria());
    }


    @Test
    public void setMateria_id() {
        materia.setMateriaId(2);
        assertEquals(2, materia.getMateriaId());
    }
}