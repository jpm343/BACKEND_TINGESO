package grupo.uno.backend.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RespuestaAlmacenadaTest {
    @Test
    public void setRespuestaAlmacenadaId() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaAlmacenadaId(1L);
        assertEquals((Long)1L, respuestaAlmacenada.getRespuestaAlmacenadaId());
    }

    @Test
    public void setRespuestaReal() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaReal("respuesta");
        assertEquals("respuesta", respuestaAlmacenada.getRespuestaReal());
    }

    @Test
    public void setRespuestaAlumno() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaAlumno("respuesta");
        assertEquals("respuesta", respuestaAlmacenada.getRespuestaAlumno());
    }

    @Test
    public void setNumeroRespuesta() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setNumeroRespuesta(1);
        assertEquals(1, respuestaAlmacenada.getNumeroRespuesta());
    }

    @Test
    public void setPutajeRespuesta() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setPutajeRespuesta(1F);
        assertEquals((Float)1F, respuestaAlmacenada.getPutajeRespuesta());
    }

    @Test
    public void setHistorialQuiz() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        HistorialQuiz historialQuiz = new HistorialQuiz();
        respuestaAlmacenada.setHistorialQuiz(historialQuiz);
        assertNotNull(respuestaAlmacenada.getHistorialQuiz());
    }

    @Test
    public void setHistorialQuizId() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setHistorialQuizId(1L);
        assertEquals((Long)1L, respuestaAlmacenada.getHistorialQuizId());
    }

    @Test
    public void getRespuestaAlmacenadaId() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaAlmacenadaId(1L);
        assertEquals((Long)1L, respuestaAlmacenada.getRespuestaAlmacenadaId());
    }

    @Test
    public void getRespuestaReal() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaReal("respuesta");
        assertEquals("respuesta", respuestaAlmacenada.getRespuestaReal());
    }

    @Test
    public void getRespuestaAlumno() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setRespuestaAlumno("respuesta");
        assertEquals("respuesta", respuestaAlmacenada.getRespuestaAlumno());
    }

    @Test
    public void getNumeroRespuesta() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setNumeroRespuesta(1);
        assertEquals(1, respuestaAlmacenada.getNumeroRespuesta());
    }

    @Test
    public void getPutajeRespuesta() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setPutajeRespuesta(1F);
        assertEquals((Float)1F, respuestaAlmacenada.getPutajeRespuesta());
    }

    @Test
    public void getHistorialQuiz() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        respuestaAlmacenada.setHistorialQuizId(1L);
        assertEquals((Long)1L, respuestaAlmacenada.getHistorialQuizId());
    }

    @Test
    public void getHistorialQuizId() {
        RespuestaAlmacenada respuestaAlmacenada = new RespuestaAlmacenada();
        HistorialQuiz historialQuiz = new HistorialQuiz();
        respuestaAlmacenada.setHistorialQuiz(historialQuiz);
        assertNotNull(respuestaAlmacenada.getHistorialQuiz());
    }
}